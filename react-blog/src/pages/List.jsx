import React, { useState } from "react";
import AppFooter from "../components/AppFooter/AppFooter";
import AppHeader from "../components/AppHeader/AppHeader";
import AppMenu from "../components/AppMenu/AppMenu";
import BlogList from "../components/BlogList/BlogList";
import Search from "../components/BlogSearch/Search";
import { Fragment, useEffect } from "react";
import axios from "axios";
import Swal from "sweetalert2";
import {BASE_URL} from "../constant/path";


export default function List() {
  const [list, setList] = useState([]);

  useEffect(() => {
    getAllList();
  }, []);

  /**
   * get all list blog
   * @param null
   * @returns mix
   */
  const getAllList = () => {
    axios
      .get(`${BASE_URL}/`)
      .then((res) => {
        setList(res.data);
      })
      .catch((error) => console.log(error));
  };

  /**
   * search title blog
   * @param {*} textTitle
   * @returns mix
   */
  const searchTitleBlog = (textTitle) => {
    axios
      .get(`${BASE_URL}?q=${textTitle}`)
      .then((res) => {
        setList(res.data);
      })
      .catch((error) => console.log(error));
  };

  /**
   * delete posts by id
   * @param {*} id
   * @returns mix
   */
  const deletePostById = (id) => {
    Swal.fire({
      title: "Bạn có muốn xóa bài viết này không?",
      text: "",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Có",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`${BASE_URL}/${id}`).then((response) => {
          Swal.fire("Xóa thành công", "", "success");
          return getAllList();
        });
      }
    });
  };

  return (
    <div>
      <Fragment>
        <div>
          <AppHeader />
          <div className="row">
            <div className="col-xl-2 col-md-3 appmenu">
              <AppMenu />
            </div>
            <div className="col-xl-10 col-md-9 content">
              <Search searchTitleBlog={searchTitleBlog} />
              <BlogList list={list} deletePostById={deletePostById} />
            </div>
          </div>
          <AppFooter />
        </div>
      </Fragment>
    </div>
  );
}

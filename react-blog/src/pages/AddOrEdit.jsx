import React, { useState } from "react";
import AppFooter from "../components/AppFooter/AppFooter";
import AppHeader from "../components/AppHeader/AppHeader";
import AppMenu from "../components/AppMenu/AppMenu";
import { LIST_POSITION } from "../constant/listPosition";
import { LIST_CATEGORY } from "../constant/listCategory";
import { Redirect, useParams } from "react-router-dom";
import { Fragment, useEffect } from "react";
import axios from "axios";
import Swal from "sweetalert2";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {BASE_URL} from "../constant/path";
import List from "./List";
import { Router } from "react-router-dom";
import { Route } from "react-router-dom";

export default function AddOrEdit() {
  const [checkedPosition, setCheckedPosition] = useState([]);
  const [listPosition , getPosition] = useState([])
  const [checkedPublic, setCheckedPublic] = useState("");
  const [post, setPost] = useState({});
  const [textTile, setTextTile] = useState("")
  let { id } = useParams();

  useEffect(() => {
    titleBlog();
    getPostById(id);
  }, []);

  const titleBlog = () => {
    id ? setTextTile("Edit Blog") : setTextTile("New Blog")
  }
  /**
   * Get post by id
   * @param {*} id
   * @returns mix
   */
  const getPostById = (id) => {
    if (id) {
      axios
        .get(`${BASE_URL}/${id}`, {
          params: {
            id: id,
          },
        })
        .then(function (response) {
          //set data to post
          setPost(response.data);
          //set checked positon by id
          setCheckedPosition([...response.data.position]);
          //set checked public
          setCheckedPublic(response.data.public);
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  };

  /**
   * Handle check position
   * @param {*} id
   * @returns array
   */
  const handleCheckPosition = (id) => {
    setCheckedPosition((prev) => {
      const isChecked = checkedPosition.includes(id);
      if (isChecked) {
        return checkedPosition.filter((item) => item !== id);
      } else {
        return [...prev, id];
      }
    });
  };

  /**
   * Handle check public
   * @param {*} id
   * @returns number
   */
  const handleCheckPublic = (id) => {
    return setCheckedPublic(id);
  };

  /**
   * Handle submit
   * @param {*} event
   * @returns mix
   */
  const handleSubmit = (event) => {
    event.preventDefault();
    const post = {
      title: event.target.title.value,
      des: event.target.des.value,
      detail: event.target.detail.value,
      category: parseInt(event.target.category.value),
      public: JSON.parse(event.target.public.value),
      data_pubblic: event.target.data_pubblic.value,
      position: checkedPosition,
    };

    if (id) {
      axios
        .put(`${BASE_URL}/${id}`, post)
        .then((response) => {
          if (response.status === 200) {
            Swal.fire("Cập nhật", "Cập nhật bài viết thành công!", "success")
            setTimeout(()=>{
              window.location.href="/list"
            },2000)
          }
        })
        .catch((error) => {
          Object.values(error.response.data.errors).map((value, key) => {
            toast(value[0]);
          });
        });
    } else {
      axios
        .post(`${BASE_URL}`, post)
        .then((response) => {
          if (response.status === 201) {
            Swal.fire("Thêm mới", "Thêm mới bài viết thành công!", "success");
            setTimeout(()=>{
              window.location.href="/list"
            },2000)
          }
        })
        .catch((error) => {
          Object.values(error.response.data.errors).map((value, key) => {
            toast(value[0]);
          });
        });
    }
  };

  return (
    <div>
      <Fragment>
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        {/* Same as */}
        <ToastContainer />
        <div className="">
          <AppHeader />
          <div className="row">
            <div className="col-xl-2 col-md-3 appmenu">
              <AppMenu />
            </div>
            <div className="col-xl-10 col-md-9 content">
              <div className="card">
                <div className="card-header">
                  <h3>{textTile}</h3>
                </div>
                <form onSubmit={handleSubmit}>
                  <div className="card-body">
                    <div className="form-group">
                      <label>Tiêu đề</label>
                      <textarea
                        type="text"
                        name="title"
                        className="form-control"
                        defaultValue={post.title}
                      />
                    </div>
                    <br />

                    <div className="form-group">
                      <label>Mô tả ngắn</label>
                      <textarea
                        name="des"
                        className="form-control"
                        defaultValue={post.des}
                      />
                    </div>
                    <br />

                    <div className="form-group">
                      <label>Chi tiết</label>
                      <textarea
                        name="detail"
                        className="form-control"
                        defaultValue={post.detail}
                      />
                    </div>
                    <br />

                    <div className="form-group">
                      <label>Hình ảnh</label>
                      <input
                        type="file"
                        name="thumbs"
                        className="form-control"
                      />
                    </div>
                    <br />

                    <div className="form-group">
                      <label>Vị trí :</label>
                      <br />
                      {LIST_POSITION.map((value, key) => {
                        return (
                          <div
                            className="form-check form-check-inline"
                            key={key}
                          >
                            <input
                              className="form-check-input"
                              type="checkbox"
                              id={`position-${value.id}`}
                              value="option1"
                              name="position"
                              checked={checkedPosition.includes(value.id)}
                              onChange={() => handleCheckPosition(value.id)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor={`position-${value.id}`}
                            >
                              {value.name}
                            </label>
                          </div>
                        );
                      })}
                    </div>
                    <br />

                    <div className="form-group">
                      <label>Public</label>
                      <br />
                      <div className="form-check form-check-inline">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="public"
                          value="true"
                          checked={checkedPublic == true}
                          onChange={() => handleCheckPublic(true)}
                        />
                        <label className="form-check-label" >
                          Yes
                        </label>
                      </div>
                      <div className="form-check form-check-inline">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="public"
                          value="false"
                          checked={checkedPublic == false}
                          onChange={() => handleCheckPublic(false)}
                        />
                        <label className="form-check-label" >
                          No
                        </label>
                      </div>
                    </div>
                    <br />

                    <div className="row">
                      <div className="col-md-6 col-12">
                        <div className="form-group">
                          <label>Loại</label>
                          <select className="form-control" name="category">
                            {LIST_CATEGORY.map((value, key) => {
                              if (value.id == post.category) {
                                return (
                                  <option key={key} value={value.id} selected>
                                    {value.name}
                                  </option>
                                );
                              } else {
                                return (
                                  <option key={key} value={value.id}>
                                    {value.name}
                                  </option>
                                );
                              }
                            })}
                          </select>
                        </div>
                      </div>
                      <div className="col-md-6 col-12">
                        <div className="form-group">
                          <label>Date Public</label>
                          <input
                            type="date"
                            name="data_pubblic"
                            className="form-control"
                            defaultValue={post.data_pubblic}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card-footer text-center">
                    <button
                      type="submit"
                      className="btn btn-success btn-submit"
                    >
                      Submit
                    </button>
                    
                    <button type="reset" className="btn btn-primary" >
                      Clear
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <AppFooter />
        </div>
      </Fragment>
    </div>
  );
}

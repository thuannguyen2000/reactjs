import { Container, Row, Col } from "react-bootstrap";
import "./AppHeaderStyle.css";
function AppHeader() {
  return (
    <div className="header">
      <Row>
        <Col md={2} xs={6} className="text-center">
          <img
            src="http://devfast.us/images/devfast-logo.png"
            alt=""
            className="logo"
          />
        </Col>
        <Col md={10} xs={6}>
          <h2 className="name-app">Blog Management</h2>
        </Col>
      </Row>
    </div>
  );
}

export default AppHeader;

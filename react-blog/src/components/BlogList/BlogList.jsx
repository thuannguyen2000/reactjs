import { Link } from "react-router-dom";
import { LIST_CATEGORY } from "../../constant/listCategory";
import { LIST_POSITION } from "../../constant/listPosition";

export default function BlogList({ list, deletePostById }) {
  /**
   * Load category name from url by id from generated data
   *
   * @param {*} idCategory
   * @returns string
   */
  function convertCategory(id) {
    const category = LIST_CATEGORY.find(
      (categoryItem) => categoryItem.id === id
    );
    return category?.name;
  }

  /**
   * get list of position names by position id
   * @param {*} arrPosition
   * @returns string
   */
  function convertPosition(arrPosition) {
    const listPosition = []
    arrPosition.forEach((position) => {
      listPosition.push(
        LIST_POSITION.find((positionItem) => positionItem.id === position)
          .name
      )
    })
    return listPosition.join(',')
  }
  /**
   * get the post id and pass it to the deletePostById function of the list component
   * @param {*} id
   * @returns function
   */
  const deletePost = (id) => {
    return deletePostById(id);
  };

  return (
    <div>
      <div>
        <div className="card">
          <div className="card-header">
            <h3>List Blog</h3>
          </div>
          <div className="card-body">
            <div className="table-responsive">
              <table className="table table-bordered">
                <thead>
                  <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Tin</th>
                    <th scope="col">Loại</th>
                    <th scope="col">Vị trí</th>
                    <th scope="col">Trạng thái</th>
                    <th scope="col">Ngày Public</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Delele</th>
                  </tr>
                </thead>
                <tbody>
                  {list.map((value, key) => {
                    return (
                      <tr key={key}>
                        <th scope="row">{value.id}</th>
                        <td>{value.title}</td>
                        <td>{convertCategory(value.category)}</td>
                        <td>{convertPosition(value.position)}</td>
                        <td>{value.public == true ? "Yes" : "No"}</td>
                        <td>{value.data_pubblic}</td>
                        <td>
                          <Link
                            to={`/edit/${value.id}`}
                            className="btn btn-outline-success"
                          >
                            Edit
                          </Link>
                        </td>
                        <td>
                          <button
                            onClick={() => deletePost(value.id)}
                            className="btn btn-outline-danger"
                          >
                            Delete
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

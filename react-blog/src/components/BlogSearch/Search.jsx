import React from "react";
import { useState } from "react";

export default function Search({ searchTitleBlog }) {
  const [text, setText] = useState("");

  /**
   * get text and set text to useState
   * @param {*} event 
   * @returns text
   */
  const handleSearch = (event) => {
    return setText(event.target.value);
  };
  
  /**
   * Call the searchTitleBlog function of the BlogList component
   * @returns function
   */
  const searchTitle = () => {
    return searchTitleBlog(text);
  };

  return (
    <div>
      <div className="card mb-3">
        <div className="card-header">
          <h3>Search Blog</h3>
        </div>
        <div className="card-body">
          <div className="form-group">
            <label>Title</label>
            <input
              type="text"
              className="form-control"
              placeholder="Search..."
              onChange={handleSearch}
            />
          </div>
          <div className="form-group text-center">
            <button
              className="btn btn-outline-success mt-2"
              onClick={searchTitle}
            >
              Search
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

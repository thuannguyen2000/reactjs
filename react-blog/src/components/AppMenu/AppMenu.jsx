import { Link } from "react-router-dom";
import "./AppMenuStyle.css";

function AppMenu() {
  return (
    <div className="menu">
      <ul>
        <Link to="/list">
          <li>
            <i className="fas fa-list"></i> List
          </li>
        </Link>
        
        <Link to="/new">
          <li>
            <i className="far fa-plus-square"></i> New
          </li>
        </Link>
      </ul>
    </div>
  );
}

export default AppMenu;

import "./App.css";
import AppHeader from "./components/AppHeader/AppHeader";
import AppMenu from "./components/AppMenu/AppMenu";
import AppFooter from "./components/AppFooter/AppFooter";


import Routes from "./Routes";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return <Routes />;
}

export default App;

/* eslint-disable jsx-a11y/scope */
import "./BlogList.css";
import { LIST_CATEGORY } from "../../Constants/ListCategory.js";
import { LIST_POSITION } from "../../Constants/ListPosition.js";
import { Link } from "react-router-dom";

export default function BlogsList({ list, deletePostById }) {
  // const [list, setList] = useState([]);

  // useEffect(() => {
  //   getAllList();
  // }, []);

  // const getAllList = () => {
  //   axios
  //     .get(`${BASE_URL}`)
  //     .then((res) => {
  //       setList(res.data);
  //     })
  //     .catch((error) => console.log(error));
  // };

  // // const searchTitleBlog = (textTitle) => {
  // //   axios
  // //     .get(`${BASE_URL}/q=${textTitle}`)
  // //     .then((res) => {
  // //       setList(res.data);
  // //     })
  // //     .catch((error) => console.log(error));
  // // };

  function convertCategory(id) {
    const category = LIST_CATEGORY.find((cate) => cate.id === id);
    return category?.name;
  } 

  function convertPosition(arrPosition) {
    const listPosition = [];
    arrPosition.forEach((position) => {
      listPosition.push(
        LIST_POSITION.find((posi) => posi.id === position).name
      );
    });
    return listPosition.join(",");
  }

  // const handleDelete = (id) => {
  //   Swal.fire({
  //     title: 'Are you sure?',
  //     text: "You won't be able to revert this!",
  //     icon: 'warning',
  //     showCancelButton: true,
  //     confirmButtonColor: '#3085d6',
  //     cancelButtonColor: '#d33',
  //     confirmButtonText: 'Yes, delete it!'
  //   }).then((result) => {
  //     if (result.isConfirmed) {
  //       axios.delete(`${BASE_URL}/${id}`).then((res) =>{
  //         Swal.fire(
  //           'Deleted!',
  //           'Your file has been deleted.',
  //           'success'
  //         )
  //         return getAllList();
  //       })
  //     }
  //   })
  // };

  const deletePost = (id) => {
    return deletePostById(id);
  };

  return (
    <>
      <div>
        <div>
          <div className="card">
            <div className="card-header">
              <h3>List Blog</h3>
            </div>
            <div className="card-body">
              <div class="table-responsive">
                <table className="table table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">Id</th>
                      <th scope="col">Tin</th>
                      <th scope="col">Loại</th>
                      <th scope="col">Vị trí</th>
                      <th scope="col">Trạng thái</th>
                      <th scope="col">Ngày Public</th>
                      <th scope="col">Edit</th>
                      <th scope="col">Delele</th>
                    </tr>
                  </thead>
                  <tbody>
                    {list.map((value, key) => {
                      return (
                        <tr key={key}>
                          <th scope="row">{value.id}</th>
                          <td>{value.title}</td>
                          <td>{convertCategory(value.category)}</td>
                          <td>{convertPosition(value.position)}</td>
                          <td>{value.public === 1 ? "yes" : "no"}</td>
                          <td>{value.data_public}</td>
                          <td>
                            <Link
                              to={`/edit/${value.id}`}
                              className="btn btn-outline-success"
                            >
                              Edit
                            </Link>
                          </td>
                          <td>
                            <button
                              onClick={() => deletePost(value.id)}
                              className="btn btn-outline-danger"
                            >
                              Delete
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

/* eslint-disable no-unused-vars */
import React from "react";
import { useState } from "react";
import "./BlogSearch.css";

export default function BlogsSearch({searchTitleBlog}) {
  const [textSearch, setTextSearch] = useState("");

  const handleSearch = (event) => {
    setTextSearch(event.target.value);
  };

  const searchTitle = () => {
    searchTitleBlog(textSearch);
  };

  // const searchTitleBlog = (textTitle) => {
  //   axios
  //     .get(`${BASE_URL}/q=${textTitle}`)
  //     .then((res) => {
  //       setList(res.data);
  //     })
  //     .catch((error) => console.log(error));
  // };
  
  return (
    <div>
    <div className="card mb-3">
      <div className="card-header">
        <h3>Search Blog</h3>
      </div>
      <div className="card-body">
        <div class="form-group">
          <label>Title</label>
          <input
            type="text"
            class="form-control"
            placeholder="Search..."
            onChange={handleSearch}
          />
        </div>
        <div class="form-group text-center">
          <button
            className="btn btn-outline-success mt-2"
            onClick={searchTitle}
          >
            Search
          </button>
        </div>
      </div>
    </div>
  </div>
  );
}

import "./AppHeader.css";

export default function AppHeader() {
  return (
    <div className="AppHeader">
      <header className="d-flex">
        <div className="logo">   
            <img
              className="logo-img"
              src="http://devfast.us/images/devfast-logo.png"
              alt="blog.vn"
            />
        </div>
        <div className="w-100">
          <p>Blogmanagent</p>
        </div>
      </header>
    </div>
  );
}

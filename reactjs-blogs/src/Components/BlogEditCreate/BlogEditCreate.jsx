/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useParams, useEffect } from "react";
import { toast } from "react-toastify";
import axios from "axios";
import Swal from "sweetalert2";
import { LIST_POSITION } from "../../Constants/ListPosition.js";
import { LIST_CATEGORY } from "../../Constants/ListCategory.js";

export default function BlogsEditCreate() {
  const BASE_URL = "http://localhost:3000/blogs";
  const [list, setList] = useState([]);
  const [checkedPosition, setCheckedPosition] = useState([]);
  const [checkedPublic, setCheckedPublic] = useState[""];
  let { id } = useParams();

  useEffect(() => {
    getPostById(id);
  }, []);

  const getPostById = (id) => {
    if (id) {
      axios
        .get(`${BASE_URL}/${id}`, {
          params: {
            id: id,
          },
        })
        .then((res) => {
          setList(res.data);
          let dataPosition = res.data.position.split(",").map((item) => {
            return parseInt(item, 10);
          });

          setCheckedPosition([...dataPosition]);

          setCheckedPublic([res.data.public]);
        })
        .catch((error) => alert(error));
    }
  };

  const handleCheckPublic = (id) => {
    return setCheckedPublic(id);
  };

  const handleCheckPosition = (id) => {
    setCheckedPosition((prev) => {
      const isChecked = checkedPosition.includes(id);
      if (isChecked) {
        return checkedPosition.filter((item) => item !== id);
      } else {
        return [...prev, id];
      }
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const list = {
      title: e.target.title.value,
      des: e.target.des.value,
      detail: e.target.detail.value,
      category: parseInt(e.target.category.value),
      public: parseInt(e.target.public.value),
      data_public: e.target.data_public.value,
      position: checkedPosition.toString(),
      thumbs: e.target.title.value.split(/(\\|\/)/g).pop(),
    };

    if (id) {
      axios
        .put(`${BASE_URL}/${id}`, list)
        .then((res) => {
          if (res.status === 200) {
            Swal.fire("Cập nhật", "Cập nhật bài viết thành công!", "success");
          }
        })
        .catch((error) => {
          Object.values(error.res.data.errors).map((value) => {
            toast(value[0]);
          });
        });
    }
  };
  return (
    <div id="page">
      <form className="page-body">
        <div className="card">
          <h4 className="card-title">New Blog</h4>
          <form onSubmit={handleSubmit}>
            <div className="card-body">
              <div className="card-item">
                <span className="title-newpage">Tiêu đề:</span>
                <textarea
                  className="title1"
                  defaultValue={list.title}
                ></textarea>
              </div>
              <div className="card-item">
                <span className="title-newpage">Mô tả ngắn:</span>
                <textarea className="title2" defaultValue={list.des}></textarea>
              </div>
              <div className="card-item">
                <span className="title-newpage">Chi tiết:</span>
                <textarea
                  className="title3"
                  defaultValue={list.detail}
                ></textarea>
              </div>
              <div className="card-item">
                <span className="title-newpage">Hình ảnh:</span>
                <input className="btn-img border-0" type="file" />
              </div>
              <div className="card-item">
                <span className="title-newpage">Vị trí:</span>
                {LIST_POSITION.map((value, key) => {
                  return (
                    <div className="form-check" key={key}>
                      <input
                        className="form-check-input"
                        type="checkbox"
                        id={`position-${value.id}`}
                        checked={checkedPosition.includes(value.id)}
                        onChange={() => handleCheckPosition(value.id)}
                      />
                      <label
                        className="form-check-label"
                        htmlFor={`position-${value.id}`}
                      >
                        {value.id}
                      </label>
                    </div>
                  );
                })}
              </div>
              <div className="card-item">
                <span className="title-newpage">Public:</span>
                <div className="form-check form-check-inline">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="public"
                    id="1"
                    value="1"
                    checked={checkedPublic === 1}
                    onChange={() => handleCheckPublic(1)}
                  />
                  <label className="form-check-label" htmlFor="1">
                    Yes
                  </label>
                </div>
                <div className="form-check form-check-inline">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="public"
                    id="0"
                    value="0"
                    checked={checkedPublic === 0}
                    onChange={() => handleCheckPublic(0)}
                  />
                  <label className="form-check-label" htmlFor="0">
                    No
                  </label>
                </div>
              </div>
              <div className="card-item-last">
                <div className="row">
                  <div className="col-md-6 col-last">
                    <span className="title-newpage">Loại:</span>
                    <select className="form-control" name="category">
                      {LIST_CATEGORY.map((value, key) => {
                        if (value.id === list.category) {
                          return (
                            <option key={key} value={value.id} selected>
                              {value.name}
                            </option>
                          );
                        } else {
                          return (
                            <option key={key} value={value.id}>
                              {value.name}
                            </option>
                          );
                        }
                      })}
                    </select>
                  </div>
                  <div className="col-md-6 col-last">
                    <span className="title-newpage">Date Public:</span>
                    <input type="date" />
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div className="btn-submit d-flex mt-2 justify-content-center">
          <div className="m-lg-2">
            <button className="btn btn-success" type="submit">
              Submit
            </button>
          </div>
          <div className="m-lg-2">
            <button className="btn btn-primary" type="submit" value="reset">
              Clear
            </button>
          </div>
        </div>
      </form>
    </div>
  );
}

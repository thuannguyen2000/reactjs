import "./AppFooter.css";

export default function AppFooter() {
  return (
    <div className="AppFooter">
      <div className="footer-card bg-dark">
        <p>
          Copyright 2022
          <a href="https://devfast.us/" className="">DevFast</a>
        </p>
      </div>
    </div>
  );
}

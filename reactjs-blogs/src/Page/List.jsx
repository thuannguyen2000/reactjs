import axios from "axios";
import React, { Fragment, useEffect, useState } from "react";
import Swal from "sweetalert2";
import { BASE_URL } from "../../Constants/Base_Url";
import AppHeader from './Components/AppHeader/AppHeader';
import AppMenu from './Components/AppMenu/AppMenu';
import AppFooter from './Components/AppFooter/AppFooter';
import BlogsSearch from "../Components/BlogSearch/BlogSearch";
import BlogsList from "../Components/BlogList/BlogsList";
export default function List() {
  const [list, setList] = useState([]);

  useEffect(() => {
    getAllList();
  }, []);

  const getAllList = () => {
    axios
      .get(`${BASE_URL}`)
      .then((response) => {
        setList(response.data);
      })
      .catch((error) => alert(error));
  };

  const searchTitleBlog = (textTitle) => {
    axios
      .get(`${BASE_URL}/search?q=${textTitle}`)
      .then((response) => {
        setList(response.data);
      })
      .catch((error) => alert(error));
  };

  const deletePostById = (id) => {
    Swal.fire({
      title: "Bạn có muốn xóa bài viết này không?",
      text: "",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Có",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`${BASE_URL}/${id}`)
        .then((response) => {
          Swal.fire("Xóa thành công", "", "success");
          return getAllList();
        });
      }
    });
  };

  return (
      <>
      <Fragment>
          <div>
              <AppHeader />
          </div>
          <div className="container-fluid">
            <div className="row">
                <div className="col-md-2 bg-link sidebar d-flex">
                    <AppMenu />
                </div>
                <div className="col-md-10 p-0">
                    <BlogsSearch searchTitleBlog={searchTitleBlog} />
                    <BlogsList list={list} deletePostById={deletePostById}/>
                </div>
                <div>
                    <AppFooter />
                </div>
            </div>
          </div>
      </Fragment>
      </>
  )
}

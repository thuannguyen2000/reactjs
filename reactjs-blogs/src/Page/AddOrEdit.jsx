import axios from "axios";
import React, { Fragment, useEffect, useState } from "react";
import { useParams } from "react-router";
import { toast, ToastContainer } from "react-toastify";
import Swal from "sweetalert2";
import { BASE_URL } from "../../Constants/Base_Url";
import AppFooter from "../Components/AppFooter/AppFooter";
import AppHeader from "../Components/AppHeader/AppHeader";
import AppMenu from "../Components/AppMenu/AppMenu";
import {LIST_CATEGORY} from "../../Constants/ListCategory.js"
import {LIST_POSITION} from "../../Constants/ListPosition.js"

export default function AddOrEdit() {
  const [checkedPosition, setCheckedPosition] = useState([]);
  const [checkedPublic, setCheckedPublic] = useState("");
  const [post, setPost] = useState([]);

  let { id } = useParams();

  useEffect(() => {
    getPostById(id);
  }, []);

  const getPostById = (id) => {
    if (id) {
      axios
        .get(`${BASE_URL}/${id}`, {
          params: {
            id: id,
          },
        })
        .then(function (response) {
          setPost(response.data);

          let dataPosition = response.data.position
            .split(",")
            .map(function (item) {
              return parseInt(item, 10);
            });

          setCheckedPosition([...dataPosition]);

          setCheckedPublic(response.data.public);
        })
        .catch(function (error) {
          alert(error);
        });
    }
  };

  const handleCheckPosition = (id) => {
    setCheckedPosition((prev) => {
      const isChecked = checkedPosition.includes(id);
      if (isChecked) {
        return checkedPosition.filter((item) => item !== id);
      } else {
        return [...prev, id];
      }
    });
  };

  const handleCheckPublic = (id) => {
    return setCheckedPublic(id);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const post = {
      title: event.target.title.value,
      des: event.target.des.value,
      detail: event.target.detail.value,
      category: parseInt(event.target.category.value),
      public: parseInt(event.target.public.value),
      data_public: event.target.data_public.value,
      position: checkedPosition.toString(),
    };

    if (id) {
      axios
        .put(`${BASE_URL}/${id}`, post)
        .then((response) => {
          if (response.status === 200) {
            Swal.fire("Cập nhật", "Cập nhật bài viết thành công!", "success");
          }
        })
        .catch((error) => {
          Object.values(error.response.data.errors).map((value) => {
            toast(value[0]);
          });
        });
    } else {
      axios
        .post(`${BASE_URL}`, post)
        .then((response) => {
          if (response.status === 201) {
            Swal.fire("Thêm mới", "Thêm mới bài viết thành công!", "success");
          }
        })
        .catch((error) => {
          Object.values(error.response.data.errors).map((value) => {
            toast(value[0]);
          });
        });
    }
  };
  return (
    <>
      <Fragment>
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
        />
        <div>
          <AppHeader />
          <div className="row">
            <div className="col-xl-2 col-md-3">
              <AppMenu />
            </div>
            <div className="col-xl-10 col-md-9 content">
              <div className="card">
                <div className="card-header">
                  <h3>New Blog</h3>
                </div>
                <form onSubmit={handleSubmit}>
                  <div className="card-body">
                    <div className="form-group">
                      <label>Tiêu đề</label>
                      <input
                        type="text"
                        name="title"
                        className="form-control"
                        defaultValue={post.title}
                      />
                    </div>
                    <br />

                    <div className="form-group">
                      <label>Mô tả ngắn</label>
                      <textarea
                        name="des"
                        className="form-control"
                        defaultValue={post.des}
                      />
                    </div>
                    <br />

                    <div className="form-group">
                      <label>Chi tiết</label>
                      <textarea
                        name="detail"
                        className="form-control"
                        defaultValue={post.detail}
                      />
                    </div>
                    <br />

                    <div className="form-group">
                      <label>Hình ảnh</label>
                      <input
                        type="file"
                        name="thumbs"
                        className="form-control"
                      />
                    </div>
                    <br />

                    <div className="form-group">
                      <label>Vị trí {console.log(checkedPosition)}</label>
                      <br />
                      {LIST_POSITION.map((value, key) => {
                        return (
                          <div
                            className="form-check form-check-inline"
                            key={key}
                          >
                            <input
                              className="form-check-input"
                              type="checkbox"
                              id={`position-${value.id}`}
                              value="option1"
                              name="position"
                              checked={checkedPosition.includes(value.id)}
                              onChange={() => handleCheckPosition(value.id)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor={`position-${value.id}`}
                            >
                              {value.name}
                            </label>
                          </div>
                        );
                      })}
                    </div>
                    <br />

                    <div className="form-group">
                      <label>Public</label>
                      <br />
                      <div className="form-check form-check-inline">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="public"
                          id="1"
                          value="1"
                          checked={checkedPublic === 1}
                          onChange={() => handleCheckPublic(1)}
                        />
                        <label className="form-check-label" htmlFor="1">
                          Yes
                        </label>
                      </div>
                      <div className="form-check form-check-inline">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="public"
                          id="0"
                          value="0"
                          checked={checkedPublic === 0}
                          onChange={() => handleCheckPublic(0)}
                        />
                        <label className="form-check-label" htmlFor="0">
                          No
                        </label>
                      </div>
                    </div>
                    <br />

                    <div className="row">
                      <div className="col-md-6 col-12">
                        <div className="form-group">
                          <label>Loại</label>
                          <select className="form-control" name="category">
                            {LIST_CATEGORY.map((value, key) => {
                              if (value.id == post.category) {
                                return (
                                  <option key={key} value={value.id} selected>
                                    {value.name}
                                  </option>
                                );
                              } else {
                                return (
                                  <option key={key} value={value.id}>
                                    {value.name}
                                  </option>
                                );
                              }
                            })}
                          </select>
                        </div>
                      </div>
                      <div className="col-md-6 col-12">
                        <div className="form-group">
                          <label>Date Public</label>
                          <input
                            type="date"
                            name="data_public"
                            className="form-control"
                            defaultValue={post.data_public}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card-footer text-center">
                    <button
                      type="submit"
                      className="btn btn-success btn-submit"
                    >
                      Submit
                    </button>
                    <button type="reset" className="btn btn-primary">
                      Clear
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <AppFooter />
        </div>
      </Fragment>
    </>
  );
}

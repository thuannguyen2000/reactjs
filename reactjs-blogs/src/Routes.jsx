import React from 'react'
import { Route, Router, Routes } from 'react-router'
import { Router } from 'react-router-dom/cjs/react-router-dom.min';
import List from "./pages/List";
import AddOrEdit from "./pages/AddOrEdit";

export default function Routes() {
  return (
    <Router>
        <Routes>
        <Route path='/list'>
            <List />
        </Route>
        <Route path='/new'>
                <AddOrEdit />
        </Route>
        <Route path='/edit/:id'>
                <AddOrEdit />
        </Route>
    </Router>
  )
}

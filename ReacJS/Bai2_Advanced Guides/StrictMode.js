import React from "react";

function ExampleApplication() {
  return (
    <div>
      <Header />
      <React.StrictMode>
        <div>
          <ComponentOne />
          <ComponentTwo />
        </div>
      </React.StrictMode>
      <Footer />
    </div>
  );
}

/**
 *  StrictMode hiện tại hỗ trợ:
 *  - Xác định các thành phần có lifecycle không an toàn
 *  - Cảnh báo về việc sử dụng API tham chiếu chuỗi kiểu cũ
 *  - Cảnh báo về việc sử dụng findDOMNode không còn dùng nữa
 *  - Phát hiện các side-effects không mong muốn
 *  - Phát hiện Context API cũ
 */

/**
 *  Xác định các lifecycle không an toàn
 *  - Khi chế độ StrictMode được bật, React biên dịch danh sách tất cả các component
 *      bằng cách sử dụng các lifecycle không an toàn,
 *      và ghi lại một thông báo cảnh báo với thông tin về các component này
 */

// Cảnh báo về việc sử dụng API tham chiếu chuỗi kiểu cũ
// Vì các đối tượng tham chiếu được đưa vào để thay thế cho các tham chiếu chuỗi, StrictMode sẽ hiện cảnh báo về việc sử dụng các tham chiếu chuỗi.
class MyComponent extends React.Component {
  constructor(props) {
    super(props);

    this.inputRef = React.createRef();
  }

  render() {
    return <input type="text" ref={this.inputRef} />;
  }

  componentDidMount() {
    this.inputRef.current.focus();
  }
}

// Cảnh báo về việc sử dụng findDOMNode không còn dùng nữa
class MyComponent extends React.Component {
  constructor(props) {
    super(props);
    this.wrapper = React.createRef();
  }
  render() {
    return <div ref={this.wrapper}>{this.props.children}</div>;
  }
}

// Phát hiện side-effects không mong muốn
/**
 *  Về mặt khái niệm, React hoạt động theo hai giai đoạn:
 *  - Giai đoạn render xác định thay đổi nào cần được thực hiện.
 *  - Giai đoạn commit là khi React áp dụng bất kỳ thay đổi nào.
 */

/**
 *  Các lifecycle trong giai đoạn render bao gồm những phương thức sau:
 *  - constructor
 *  - componentWillMount (hoặc UNSAFE_componentWillMount)
 *  - componentWillReceiveProps (hoặc UNSAFE_componentWillReceiveProps)
 *  - componentWillUpdate (hoặcor UNSAFE_componentWillUpdate)
 *  - getDerivedStateFromProps
 *  - shouldComponentUpdate
 *  - render
 *  - setState hàm cập nhật (tham số đầu tiên)
 */

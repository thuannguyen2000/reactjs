/**
 *  Context được thiết kế để chia sẽ data khi chúng được xem là “global data” của toàn bộ ứng dụng React,
 *  chẳng hạn như thông tin về user hiện tại đang đăng nhập, theme,
 *  hoặc ngôn ngữ mà người dùng đã chọn.
 */

// chúng ta có thể tránh được việc truyền props qua các elements trung gian:

// Context lets us pass a value deep into the component tree
// without explicitly threading it through every component.
// Create a context for the current theme (with "light" as the default).
const ThemeContext = React.createContext("light");

class App extends React.Component {
  render() {
    // Use a Provider to pass the current theme to the tree below.
    // Any component can read it, no matter how deep it is.
    // In this example, we're passing "dark" as the current value.
    return (
      <ThemeContext.Provider value="dark">
        <Toolbar />
      </ThemeContext.Provider>
    );
  }
}

// A component in the middle doesn't have to
// pass the theme down explicitly anymore.
function Toolbar() {
  return (
    <div>
      <ThemedButton />
    </div>
  );
}

class ThemedButton extends React.Component {
  // Assign a contextType to read the current theme context.
  // React will find the closest theme Provider above and use its value.
  // In this example, the current theme is "dark".
  static contextType = ThemeContext;
  render() {
    return <Button theme={this.context} />;
  }
}

// Trước khi bạn sử dụng Context
`function Page(props) {
    const user = props.user;
    const userLink = (
      <Link href={user.permalink}>
        <Avatar user={user} size={props.avatarSize} />
      </Link>
    );
    return <PageLayout userLink={userLink} />;
  }
  
  // Now, we have:
  <Page user={user} avatarSize={avatarSize} />
  // ... được renders ...
  <PageLayout userLink={...} />
  // ... được renders ...
  <NavigationBar userLink={...} />
  // ... được renders ...
  {props.userLink}`;

/**
 *  API
 */
// React.createContext
const MyContext = React.createContext(defaultValue);

// Context.Provider
<MyContext.Provider value={"/* some value */"}></MyContext.Provider>;

// Class.contextType
class MyClass extends React.Component {
  componentDidMount() {
    let value = this.context;
    /* Thực hiện một side-effect tại mount sử dụng giá trị của MyContext */
  }
  componentDidUpdate() {
    let value = this.context;
    /* ... */
  }
  componentWillUnmount() {
    let value = this.context;
    /* ... */
  }
  render() {
    let value = this.context;
    /* render thứ gì đó dựa vào giá trị của MyContext */
  }
}
MyClass.contextType = MyContext;

// Context.Consumer
<MyContext.Consumer>
  {(value) => "/* render gì đó dựa vào context value */"}
</MyContext.Consumer>;

// Context.displayName
const MyContext2 = React.createContext(/* vài giá trị */);
MyContext.displayName = "MyDisplayName";

("<MyContext.Provider>"); // "MyDisplayName.Provider" in DevTools
("<MyContext.Consumer>"); // "MyDisplayName.Consumer" in DevTools

/**
 *  Examples
 */
// Dynamic Context
import { ThemeContext, themes } from "./theme-context";
import ThemedButton from "./themed-button";

// An intermediate component that uses the ThemedButton
function Toolbar(props) {
  return <ThemedButton onClick={props.changeTheme}>Change Theme</ThemedButton>;
}

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      theme: themes.light,
    };

    this.toggleTheme = () => {
      this.setState((state) => ({
        theme: state.theme === themes.dark ? themes.light : themes.dark,
      }));
    };
  }

  render() {
    // The ThemedButton button inside the ThemeProvider
    // uses the theme from state while the one outside uses
    // the default dark theme
    return (
      <Page>
        <ThemeContext.Provider value={this.state.theme}>
          <Toolbar changeTheme={this.toggleTheme} />
        </ThemeContext.Provider>
        <Section>
          <ThemedButton />
        </Section>
      </Page>
    );
  }
}

ReactDOM.render(<App />, document.root);

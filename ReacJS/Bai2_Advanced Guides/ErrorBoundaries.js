/**
 *  Ranh giới lỗi là các thành phần React bắt lỗi JavaScript ở bất kỳ đâu trong cây thành phần con của chúng,
 *  ghi lại các lỗi đó và hiển thị giao diện người dùng dự phòng thay vì cây thành phần bị lỗi.
 *  Các ranh giới lỗi bắt lỗi trong quá trình kết xuất,
 *  trong các phương thức vòng đời và trong các hàm tạo của toàn bộ cây bên dưới chúng.
 */

/**
 *  Ranh giới lỗi không bắt lỗi đối với:
 *  - Trình xử lý sự kiện ( tìm hiểu thêm )
 *  - Mã không đồng bộ (ví dụ: setTimeouthoặc requestAnimationFramegọi lại)
 *  - Kết xuất phía máy chủ
 *  - Lỗi được tạo ra trong chính ranh giới lỗi (chứ không phải con của nó)
 */
class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    // You can also log the error to an error reporting service
    logErrorToMyService(error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <h1>Something went wrong.</h1>;
    }

    return this.props.children;
  }
}
<ErrorBoundary>
  <MyWidget />
</ErrorBoundary>;

// Đây là 2 container cùng cấp trong DOM
const appRoot = document.getElementById("app-root");
const modalRoot = document.getElementById("modal-root");

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.el = document.createElement("div");
  }

  componentDidMount() {
    // Phần tử Portals được chèn vào cây DOM sau khi
    // phần tử con của Modal được hiển thị, có nghĩa là những phần tử con đó
    // sẽ được gắn trên một phần tử DOM tách rời độc lập. Nếu một phần tử con
    // yêu cầu được gắn vào DOM tree ngay tức khắc khi 'mounted',
    // ví dụ để đo lường thuộc tính DOM, hoặc sử dụng 'autoFocus'
    // trong các phần tử con, thêm state vào Modal và
    // chỉ render các phẩn tử con khi Modal
    // được chèn vào DOM tree.
    modalRoot.appendChild(this.el);
  }

  componentWillUnmount() {
    modalRoot.removeChild(this.el);
  }

  render() {
    return ReactDOM.createPortal(this.props.children, this.el);
  }
}

class Parent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { clicks: 0 };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    // Hàm này sẽ kích hoạt khi button tại Child được click,
    // cập nhật Parent's state, mặc dù button
    // không phải là phần tử con trực tiếp trong DOM.
    this.setState((state) => ({
      clicks: state.clicks + 1,
    }));
  }

  render() {
    return (
      <div onClick={this.handleClick}>
        <p>Số lượng clicks: {this.state.clicks}</p>
        <p>
          Mở DevTools của trình duyệt để quan sát rằng button không phải con của
          div xử lý sự kiện onClick.
        </p>
        <Modal>
          <Child />
        </Modal>
      </div>
    );
  }
}

function Child() {
  // Sự kiện nhấp chuột vào nút này sẽ xuất hiện đối với phần tử cha chứa nó
  // bởi vì không có thuộc tính 'onClick' được định nghĩa
  return (
    <div className="modal">
      <button>Click</button>
    </div>
  );
}

ReactDOM.render(<Parent />, appRoot);

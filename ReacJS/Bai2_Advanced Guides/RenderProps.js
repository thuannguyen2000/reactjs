// Render props
function List({ data, children }) {
  return <ul>{data.map((item, index) => children(item, index))}</ul>;
}

function App() {
  const cars = ["Mercedes", "Vios", "Santafe"];
  return (
    <div id="wapper">
      <List data={cars}>{(item, index) => <li key={index}>{item}</li>}</List>
    </div>
  );
}
ReactDOM.render(<App />, document.getElementById("app"));

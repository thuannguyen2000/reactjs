// Profiler component có thể sử dụng lồng nhau để phân tích các component khác nhau trong cùng một subtree:
render(
  <App>
    <Profiler id="Panel" onRender={callback}>
      <Panel {...props}>
        <Profiler id="Content" onRender={callback}>
          <Content {...props} />
        </Profiler>
        <Profiler id="PreviewPane" onRender={callback}>
          <PreviewPane {...props} />
        </Profiler>
      </Panel>
    </Profiler>
  </App>
);

//onRender Callback
function onRenderCallback(
  id, // "id" của Profiler tree vừa thực hiện thay đổi
  phase, // một trong hai "mount" (nếu tree vừa được mounted) hoặc "update" (nếu nó re-rendered)
  actualDuration, // thời gian để rendering cập nhật mới
  baseDuration, //  thời gian ước tính để hiển thị toàn bộ subtree mà không cần ghi nhớ
  startTime, // khi React bắt đầu hiển thị bản cập nhật này
  commitTime, // khi React hoàn thành cập nhật
  interactions // tập hợp các tương tác thuộc về bản cập nhật
) {
  // Tổng hợp hoặc log thời gian render...
}

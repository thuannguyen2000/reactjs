// Elements Of Different Types (Khác loại)
`<div>
  <Counter />
</div>

<span>
  <Counter />
</span>`// DOM Elements Of The Same Type (Cùng loại)
`<div className="before" title="stuff" />

<div className="after" title="stuff" />`// Keys
`<ul>
  <li key="2015">Duke</li>
  <li key="2016">Villanova</li>
</ul>

<ul>
  <li key="2014">Connecticut</li>
  <li key="2015">Duke</li>
  <li key="2016">Villanova</li>
</ul>
<li key={item.id}>{item.name}</li>`;

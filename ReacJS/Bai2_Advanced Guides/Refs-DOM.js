/**
 *  Một vài trường hợp hữu ích để sử dụng refs:
 *  - Quản lý focus, text selection, hoặc media playback.
 *  - Trigger animation của một element khác.
 *  - Tích hợp những thư viện DOM từ bên thứ ba.
 *
 *  @Lưu_ý : Tránh sử dụng refs trong trường hợp chúng ta có thể khai báo.
 *   - Thay vì hiển thị phương thức open() và close() trong Dialog component,
 *   thì chúng ta sẽ sử dụng isOpen như một prop để xử lý nó.
 */

// Tạo Refs
// - Refs được khởi tạo bằng React.createRef()
//      và được gắn vào các React element thông qua thuộc tính ref.
class MyComponent extends React.Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }
  render() {
    return <div ref={this.myRef} />;
  }
}

// Truy cập Refs
// - Khi một element có chứa ref render,
//      chúng ta có thể sử dụng một thuộc tính của ref là current để truy cập đến node hiện tại.
`const node = this.myRef.current;`;

// Thêm Ref vào một DOM Element
class CustomTextInput extends React.Component {
  constructor(props) {
    super(props);
    // Tạo ra một ref để lưu textInput DOM element
    this.textInput = React.createRef();
    this.focusTextInput = this.focusTextInput.bind(this);
  }

  focusTextInput() {
    // Explicitly focus the text input using the raw DOM API
    // Note: Chúng ra truy cập đến "current" để lấy DOM node
    this.textInput.current.focus();
  }

  render() {
    // Nói với React chúng ta muốn liên kết tới <input> ref
    // Với `textInput` chúng ta đã tạo ở constructor
    return (
      <div>
        <input type="text" ref={this.textInput} />
        <input
          type="button"
          value="Focus the text input"
          onClick={this.focusTextInput}
        />
      </div>
    );
  }
}

// Thêm Ref vào Class Component
class AutoFocusTextInput extends React.Component {
  constructor(props) {
    super(props);
    this.textInput = React.createRef();
  }

  componentDidMount() {
    this.textInput.current.focusTextInput();
  }

  render() {
    return <CustomTextInput ref={this.textInput} />;
  }
}

// Refs và Function Components
// - Bạn có thể sử dụng thuộc tính ref bên trong function component
//      miễn là bạn tham chiếu đến phần tử DOM hoặc class component:
function CustomTextInput(props) {
  // textInput phải được khai báo ở đây để ref có thể tham chiếu đến nó
  const textInput = useRef(null);

  function handleClick() {
    textInput.current.focus();
  }

  return (
    <div>
      <input type="text" ref={textInput} />
      <input type="button" value="Focus the text input" onClick={handleClick} />
    </div>
  );
}

// Callback Refs
// Ví dụ dưới đây thực hiện việc sử dụng một ref callback để lưu trữ tham chiếu tới một DOM node trong một thuộc tính instance.
class CustomTextInput extends React.Component {
  constructor(props) {
    super(props);

    this.textInput = null;

    this.setTextInputRef = (element) => {
      this.textInput = element;
    };

    this.focusTextInput = () => {
      // Focus the text input using the raw DOM API
      if (this.textInput) this.textInput.focus();
    };
  }

  componentDidMount() {
    // autofocus the input on mount
    this.focusTextInput();
  }

  render() {
    // Use the `ref` callback to store a reference to the text input DOM
    // element in an instance field (for example, this.textInput).
    return (
      <div>
        <input type="text" ref={this.setTextInputRef} />
        <input
          type="button"
          value="Focus the text input"
          onClick={this.focusTextInput}
        />
      </div>
    );
  }
}

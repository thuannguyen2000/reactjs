React.createElement(
    MyButton,
    {color: 'blue', shadowSize: 2},
    'Click Me'
  )

//   React phải nằm trong Scope
import React from 'react';
import CustomButton from './CustomButton';

function WarningButton() {
  // return React.createElement(CustomButton, {color: 'red'}, null);
  return <CustomButton color="red" />;
}

// Sử Dụng Ký Hiệu Chấm cho JSX Type

import React from 'react';

const MyComponents = {
  DatePicker: function DatePicker(props) {
    return <div>Imagine a {props.color} datepicker here.</div>;
  }
}

function BlueDatePicker() {
  return <MyComponents.DatePicker color="blue" />;
}

// Component Người Dùng Tự Định Nghĩa Phải Được Viết Hoa
import React from 'react';

// Chính xác! Đây là một component và nên được viết hoa:
function Hello(props) {
  // Chính xác! Sử dụng <div> là hợp lệ vì div là một thẻ HTML:
  return <div>Hello {props.toWhat}</div>;
}

function HelloWorld() {
  // Chính xác! React biết <Hello /> là một component vì nó được viết hoa.
  return <Hello toWhat="World" />;
}


// Chọn Kiểu tại Thời Điểm Thực Thi

import React from 'react';
import { PhotoStory, VideoStory } from './stories';

const components = {
  photo: PhotoStory,
  video: VideoStory
};

function Story(props) {
  // Chính xác! JSX type có thể là một biến được viết hoa.
  const SpecificStory = components[props.storyType];
  return <SpecificStory story={props.story} />;
}

// Props trong JSX
function NumberDescriber(props) {
    let description;
    if (props.number % 2 == 0) {
      description = <strong>even</strong>;
    } else {
      description = <i>odd</i>;
    }
    return <div>{props.number} is an {description} number</div>;
  }

//   Spread Attributes
function App1() {
    return <Greeting firstName="Ben" lastName="Hector" />;
  }
  
  function App2() {
    const props = {firstName: 'Ben', lastName: 'Hector'};
    return <Greeting {...props} />;
  }

//   Spread Attributes
const Button = props => {
    const { kind, ...other } = props;
    const className = kind === "primary" ? "PrimaryButton" : "SecondaryButton";
    return <button className={className} {...other} />;
  };
  
  const App = () => {
    return (
      <div>
        <Button kind="primary" onClick={() => console.log("clicked!")}>
          Hello World!
        </Button>
      </div>
    );
  };

//   
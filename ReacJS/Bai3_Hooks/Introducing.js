// Với Hooks, bạn có sử dụng state và các chức năng khác của một React class component
// mà không cần phải viết một class.
import React, { useState } from "react";

function Example() {
  // Khai báo 1 biến trạng thái mới đặt tên là "count"
  const [count, setCount] = useState(0);

  return (
    <div>
      <p>Bạn đã bấm {count} lần</p>
      <button onClick={() => setCount(count + 1)}>Bấm vào tôi</button>
    </div>
  );
}

/**
 *  State Hook
 */
// Ví dụ này làm một bộ đếm. Khi bạn bấm vào nút, giá trị sẽ tăng 1:
import React, { useState } from "react";

function Example() {
  // Khai báo 1 biến số đếm, gọi là "count"
  const [count, setCount] = useState(0);

  return (
    <div>
      <p>Bạn đã bấm {count} lần</p>
      <button onClick={() => setCount(count + 1)}>Bấm vào tôi</button>
    </div>
  );
}

// Khai báo nhiều biến trạng thái
function ExampleWithManyStates() {
  // Khai báo nhiều biến trạng thái!
  const [age, setAge] = useState(42);
  const [fruit, setFruit] = useState("chuối");
  const [todos, setTodos] = useState([{ text: "Học Hooks" }]);
  // ...
}

/**
 *  Effect Hook
 */
/**
 *  Effect Hook, useEffect, thêm khả năng để thực hiện side effects từ các components dạng hàm.
 *  Nó phục vụ cùng mục đích như componentDidMount, componentDidUpdate,
 *  và componentWillUnmount trong React classes, nhưng thống nhất lại trong một API duy nhất.
 */

// các components này cài đặt tiêu đề của trang web sau khi React cập nhật DOM:
import React, { useState, useEffect } from "react";

function Example() {
  const [count, setCount] = useState(0);

  // Giống componentDidMount và componentDidUpdate:
  useEffect(() => {
    // Cập nhật tiêu đề trang web sử dụng API trình duyệt
    document.title = `Bạn đã bấm ${count} lần`;
  });

  return (
    <div>
      <p>Bạn đã bấm {count} lần</p>
      <button onClick={() => setCount(count + 1)}>Bấm vào tôi</button>
    </div>
  );
}

/**
 *  Quy tắc của Hooks
 */
/**
 *  Hooks là các hàm Javascript, nhưng nó bắt buộc thêm hai quy tắc:
 *  - Chỉ gọi Hooks trên cùng. Không gọi Hooks bên trong vòng lặp, điều kiện, hoặc các hàm lồng nhau.
 *  - Chỉ gọi Hooks từ các React components dạng hàm. Không gọi Hooks từ hàm JavaScript bình thường.
 *       (Chỉ có một chỗ khác đúng để gọi Hooks — Hooks tuỳ chọn của bạn.
 *           Chúng ta sẽ học vào chúng sau.)
 */

/**
 *  Xây dựng Hooks của bạn
 */
import React, { useState, useEffect } from "react";

function useFriendStatus(friendID) {
  const [isOnline, setIsOnline] = useState(null);

  function handleStatusChange(status) {
    setIsOnline(status.isOnline);
  }

  useEffect(() => {
    ChatAPI.subscribeToFriendStatus(friendID, handleStatusChange);
    return () => {
      ChatAPI.unsubscribeFromFriendStatus(friendID, handleStatusChange);
    };
  });

  return isOnline;
}
function FriendStatus(props) {
  const isOnline = useFriendStatus(props.friend.id);

  if (isOnline === null) {
    return "Đang tải...";
  }
  return isOnline ? "Online" : "Offline";
}
function FriendListItem(props) {
  const isOnline = useFriendStatus(props.friend.id);

  return (
    <li style={{ color: isOnline ? "green" : "black" }}>{props.friend.name}</li>
  );
}

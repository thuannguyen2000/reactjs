/**
 *  Chỉ gọi Hook ở trên cùng
 *  - Không gọi hook bên trong loop, câu điều kiện, hay các function lồng với nhau.
 *  - Thay vì đó, luôn sử dụng Hook ở phần trên cùng của React function, trước bất cứ việc trả về (return) nào
 */

/**
 *  Chỉ gọi Hook từ React Function
 *  Không gọi Hook từ mà function JavaScript. Thay vì đó, bạn có thể:
 *  -  Gọi Hook từ React function components.
 *  - Gọi Hook từ custom Hook
 */

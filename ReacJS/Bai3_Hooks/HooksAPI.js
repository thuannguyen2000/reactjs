// Functional updates
function Counter({ initialCount }) {
  const [count, setCount] = useState(initialCount);
  return (
    <>
      Bộ đếm: {count}
      <button onClick={() => setCount(initialCount)}>Chạy lại</button>
      <button onClick={() => setCount((prevCount) => prevCount - 1)}>-</button>
      <button onClick={() => setCount((prevCount) => prevCount + 1)}>+</button>
    </>
  );
}
// Lazy initial state
const [state, setState] = useState(() => {
  const initialState = someExpensiveComputation(props);
  return initialState;
});

// Loại bỏ một effect (Cleaning up an effect)
useEffect(() => {
  const subscription = props.source.subscribe();
  return () => {
    // Loại bỏ subscription
    subscription.unsubscribe();
  };
});
//Thực thi có điều kiện của một effect
useEffect(() => {
  const subscription = props.source.subscribe();
  return () => {
    subscription.unsubscribe();
  };
}, [props.source]);
// Ví dụ đầy đủ của Context.Provider
const themes = {
  light: {
    foreground: "#000000",
    background: "#eeeeee",
  },
  dark: {
    foreground: "#ffffff",
    background: "#222222",
  },
};

const ThemeContext = React.createContext(themes.light);

function App() {
  return (
    <ThemeContext.Provider value={themes.dark}>
      <Toolbar />
    </ThemeContext.Provider>
  );
}

function Toolbar(props) {
  return (
    <div>
      <ThemedButton />
    </div>
  );
}

function ThemedButton() {
  const theme = useContext(ThemeContext);
  return (
    <button style={{ background: theme.background, color: theme.foreground }}>
      Tôi đang được style bởi theme context!
    </button>
  );
}

/**
 *  Bổ sung về Hooks
 */
// Chỉ định state ban đầu
const [state2, dispatch] = useReducer(reducer, { count: initialCount });
// Lazy initialization
function init(initialCount) {
  return { count: initialCount };
}

function reducer(state, action) {
  switch (action.type) {
    case "increment":
      return { count: state.count + 1 };
    case "decrement":
      return { count: state.count - 1 };
    case "reset":
      return init(action.payload);
    default:
      throw new Error();
  }
}

function Counter({ initialCount }) {
  const [state, dispatch] = useReducer(reducer, initialCount, init);
  return (
    <>
      Bộ đếm: {state.count}
      <button
        onClick={() => dispatch({ type: "reset", payload: initialCount })}
      >
        Đặt lại
      </button>
      <button onClick={() => dispatch({ type: "decrement" })}>-</button>
      <button onClick={() => dispatch({ type: "increment" })}>+</button>
    </>
  );
}
// useCallback
const memoizedCallback = useCallback(() => {
  doSomething(a, b);
}, [a, b]);
// useMemo
const memoizedValue = useMemo(() => computeExpensiveValue(a, b), [a, b]);
// useRef
const refContainer = useRef(initialValue);
// Một số trường hợp cơ bản để truy cập vào các lệnh của component con:
function TextInputWithFocusButton() {
  const inputEl = useRef(null);
  const onButtonClick = () => {
    // `current` trỏ vào element text input đã được mount
    inputEl.current.focus();
  };
  return (
    <>
      <input ref={inputEl} type="text" />
      <button onClick={onButtonClick}>Đưa con trỏ vào ô input</button>
    </>
  );
}

// useImperativeHandle
function FancyInput(props, ref) {
  const inputRef = useRef();
  useImperativeHandle(ref, () => ({
    focus: () => {
      inputRef.current.focus();
    },
  }));
  return <input ref={inputRef} />;
}
FancyInput = forwardRef(FancyInput);

// useLayoutEffect
// Có đặc điểm giống với useEffect, nhưng sẽ kích hoạt đồng bộ sau khi tất cả DOM thay đổi.

// useDebugValue
function useFriendStatus(friendID) {
  const [isOnline, setIsOnline] = useState(null);

  // ...

  // Hiển thị label trong DevTools ngay cạnh Hook
  // e.g. "FriendStatus: Online"
  useDebugValue(isOnline ? "Online" : "Offline");

  return isOnline;
}

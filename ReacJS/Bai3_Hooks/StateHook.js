// Khai báo một biến state

// Trong một class, chúng ta khởi tạo count state về 0
// bằng cách cài đặt this.state về { count: 0 } bên trong constructor:
class Example extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
    };
  }
}

/**
 *  Bên trong một function component, chúng ta không có this,
 *  cho nên chúng ta không thể cài đặt hoặc đọc this.state.
 *  Thay vào đó, chúng ta gọi useState Hook trực tiếp bên trong component:
 */
import React, { useState } from "react";

function Example() {
  // Khai báo một biến state mới, gọi nó là "count"
  const [count, setCount] = useState(0);
}

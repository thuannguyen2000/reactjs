// Render nhiều component
const numbers = [1, 2, 3, 4, 5];
const listItems = numbers.map((number) =>
  <li>{number}</li>
); 
ReactDOM.render(
    <ul>{listItems}</ul>,
    document.getElementById('root')
  );

//Component Có Danh Sách Cơ Bản (Basic List Component)
function NumberList(props) {
    const numbers2 = props.numbers;
    const listItems = numbers2.map((number) =>
      <li>{number}</li>
    );
    return (
      <ul>{listItems}</ul>
  
    );
  }
  
  const numbers2 = [1, 2, 3, 4, 5];
  ReactDOM.render(
    <NumberList numbers={numbers2} />,
    document.getElementById('root')
  );

//   Chia nhỏ component vs Key
// Key chưa xác định
function ListItem(props) {
    const value = props.value;
    return (
      // Sai! Ở đây không cần truyền vào key:
      <li key={value.toString()}>
        {value}
      </li>
    );
  }
  
  function NumberList(props) {
    const numbers = props.numbers;
    const listItems = numbers.map((number) =>
      // Sai! Key nên được truyền vào ở đây:
      <ListItem value={number} />
    );
    return (
      <ul>
        {listItems}
      </ul>
    );
  }
  
  const numbers3 = [1, 2, 3, 4, 5];
  ReactDOM.render(
    <NumberList numbers={numbers3} />,
    document.getElementById('root')
  );

//   Trường hợp xác định key
function ListItem(props) {
    // Đúng! Ở đây không cần cụ thể key:
    return <li>{props.value}</li>;
  }
  
  function NumberList(props) {
    const numbers = props.numbers;
    const listItems = numbers.map((number) =>
      // Đúng! Key nên được xác định bên trong mảng:
      <ListItem key={number.toString()} value={number} />
    );
    return (
      <ul>
        {listItems}
      </ul>
    );
  }
  
  const numbers5 = [1, 2, 3, 4, 5];
  ReactDOM.render(
    <NumberList numbers={numbers5} />,
    document.getElementById('root')
  );

//   Các key bắt buộc là duy nhất giữa các nút ae
function Blog(props) {
    const sidebar = (
      <ul>
        {props.posts.map((post) =>
          <li key={post.id}>
            {post.title}
          </li>
        )}
      </ul>
    );
    const content = props.posts.map((post) =>
      <div key={post.id}>
        <h3>{post.title}</h3>
        <p>{post.content}</p>
      </div>
    );
    return (
      <div>
        {sidebar}
        <hr />
        {content}
      </div>
    );
  }
  
  const posts = [
    {id: 1, title: 'Hello World', content: 'Welcome to learning React!'},
    {id: 2, title: 'Installation', content: 'You can install React from npm.'}
  ];
  ReactDOM.render(
    <Blog posts={posts} />,
    document.getElementById('root')
  );

//   Nhúng map() vào JSX
function NumberList(props) {
    const numbers = props.numbers;
    return (
      <ul>
        {numbers.map((number) =>
          <ListItem key={number.toString()}
                    value={number} />
        )}
      </ul>
    );
  }
class Toggle extends React.Component {
    constructor(props) {
      super(props);
      this.state = {isToggleOn: true};
  
      // Phép "ràng buộc" (bind) này là cần thiết để `this` hoạt động trong callback
      this.handleClick = this.handleClick.bind(this);
    }
  
    handleClick() {
      this.setState(prevState => ({
        isToggleOn: !prevState.isToggleOn
      }));
    }
  
    render() {
      return (
        <button onClick={this.handleClick}>
          {this.state.isToggleOn ? 'ON' : 'OFF'}
        </button>
      );
    }
  }
  
  ReactDOM.render(
    <Toggle />,
    document.getElementById('root')
  );

//   Ví dụ 2
class LoggingButton extends React.Component {
    // Cú pháp này đảm bảo `this` được ràng buộc trong handleClick.
    // Lưu ý: đây là cú pháp *thử nghiệm*.
    handleClick = () => {
      console.log('this is:', this);
    }
  
    render() {
      return (
        <button onClick={this.handleClick}>
          Click me
        </button>
      );
    }
  }

// Ví dụ 3
// Truyền tham số vào hàm bắt sự kiện
{/* <button onClick={(e) => this.deleteRow(id, e)}>Delete Row</button> */}
{/* <button onClick={this.deleteRow.bind(this, id)}>Delete Row</button> */}
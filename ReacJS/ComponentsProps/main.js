// function Welcome(props) {
//     return <h1>Hello, {props.name}</h1>;
// }

// // Es6
// class Welcome extends React.Component {
//     render() {
//       return <h1>Hello, {this.props.name}</h1>;
//     }
//   }

// ví dụ 3
//   function Welcome(props) {
//     return <h1>Hello, {props.name}</h1>;
//   }
  
//   const element = <Welcome name="Thuận" />;
//   ReactDOM.render(
//     element,
//     document.getElementById('root')
//   );

//   ví dụ 4
function Welcome(props) {
    return <h1>Hello, {props.name}</h1>;
}

function App() {
    return (
        <div>
            <Welcome name="Thuận"/>
            <Welcome name="Lưu"/>
            <Welcome name="Định"/>
        </div>
    );
}

ReactDOM.render(
    <App />, document.getElementById('root')
)
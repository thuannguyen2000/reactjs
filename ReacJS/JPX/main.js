// const name = 'John Perez';
// const element = <h1>Hello , {name}</h1>;

// ReactDom.render(element, document.getElementById('root'))

// Ví dụ 2

function formatName(user) {
    return user.firstName + ' ' + user.lastName;
  }
  
  const user = {
    firstName: 'Thuận',
    lastName: 'Nguyễn'
  };
  
  const element = (
    <h1>
      Hello, {formatName(user)}!
    </h1>
  );
  
  ReactDOM.render(
    element,
    document.getElementById('root')
  );

  function getGreeting(user) {
    if (user) {
      return <h1>Hello, {formatName(user)}!</h1>;
    }
    return <h1>Hello, Stranger.</h1>;
  }

  
  
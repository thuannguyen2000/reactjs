class CustomTextInput extends React.Component {
    constructor(props) {
      super(props);
      // Tạo ra một ref để lưu textInput DOM element
      this.textInput = React.createRef();
      this.focusTextInput = this.focusTextInput.bind(this);
    }
  
    focusTextInput() {
      // Explicitly focus the text input using the raw DOM API
      // Note: Chúng ra truy cập đến "current" để lấy DOM node
      this.textInput.current.focus();
    }
  
    render() {
      // Nói với React chúng ta muốn liên kết tới <input> ref
      // Với `textInput` chúng ta đã tạo ở constructor
      return (
        <div>
          <input
            type="text"
            ref={this.textInput} />
          <input
            type="button"
            value="Focus the text input"
            onClick={this.focusTextInput}
          />
        </div>
      );
    }
  }

//   
class AutoFocusTextInput extends React.Component {
    constructor(props) {
      super(props);
      this.textInput = React.createRef();
    }
  
    componentDidMount() {
      this.textInput.current.focusTextInput();
    }
  
    render() {
      return (
        <CustomTextInput ref={this.textInput} />
      );
    }
  }
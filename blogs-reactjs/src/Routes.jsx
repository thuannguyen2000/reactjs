import { Route } from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";
import List from "./pages/List";
import AddOrEdit from "./pages/AddOrEdit";

function Routes() {
  return (
      <Router>
        <Route path="/list">
          <List />
        </Route>
        <Route path="/new">
          <AddOrEdit />
        </Route>
        <Route path="/edit/:id">
          <AddOrEdit />
        </Route>
      </Router>

  );
}

export default Routes;

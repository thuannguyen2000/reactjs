import "./AppFooterStyle.css";

function AppFooter() {
  return (
    <div className="footer">
      <div className="text-center">Copyright © 2021 DevFast</div>
    </div>
  );
}

export default AppFooter;

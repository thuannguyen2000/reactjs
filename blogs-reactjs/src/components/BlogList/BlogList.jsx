
import { Link } from "react-router-dom";
import { LIST_CATEGORY } from "../../constant/listCategory";
import { LIST_POSITION } from "../../constant/listPosition";
export default function BlogList({ list, deletePostById }) {
  /**
   * Load category name from url by id from generated data
   *
   * @param {*} idCategory
   * @returns string
   */
  const loadCategoryById = (valueCategoryByUrl) => {
    for (let categoryById of LIST_CATEGORY) {
      if (categoryById.id === valueCategoryByUrl) {
        return categoryById.name;
      }
    }
  };

  /**
   * get list of position names by position id
   * @param {*} listPostion
   * @returns string
   */
  const loadPositionById = (listPostion) => {
    listPostion = listPostion.split(",");
    return LIST_POSITION.map((position, key) => {
      return listPostion.map((positionValue) => {
        if (positionValue === position.id) {
          return `${position.name} `;
        }
      });
    });
  };

  /**
   * get the post id and pass it to the deletePostById function of the list component
   * @param {*} id 
   * @returns function
   */
  const deletePost = (id) => {
    return deletePostById(id);
  };

  return (
    <div>
      <div>
        <div className="card">
          <div className="card-header">
            <h3>List Blog</h3>
          </div>
          <div className="card-body">
            <div class="table-responsive">
              <table className="table table-bordered">
                <thead>
                  <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Tin</th>
                    <th scope="col">Loại</th>
                    <th scope="col">Vị trí</th>
                    <th scope="col">Trạng thái</th>
                    <th scope="col">Ngày Public</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Delele</th>
                  </tr>
                </thead>
                <tbody>
                  {list.map((value, key) => {
                    return (
                      <tr key={key}>
                        <th scope="row">{value.id}</th>
                        <td>{value.title}</td>
                        <td>{loadCategoryById(value.category)}</td>
                        <td>{loadPositionById(value.position)}</td>
                        <td>{value.public === 1 ? "yes" : "no"}</td>
                        <td>{value.data_pubblic}</td>
                        <td>
                          <Link
                            to={`/edit/${value.id}`}
                            className="btn btn-outline-success"
                          >
                            Edit
                          </Link>
                        </td>
                        <td>
                          <button
                            onClick={() => deletePost(value.id)}
                            className="btn btn-outline-danger"
                          >
                            Delete
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

import React, { useState } from "react";
import AppFooter from "../components/AppFooter/AppFooter";
import AppHeader from "../components/AppHeader/AppHeader";
import AppMenu from "../components/AppMenu/AppMenu";
import { LIST_POSITION } from "../constant/listPosition";
import { LIST_CATEGORY } from "../constant/listCategory";
import {  useParams } from "react-router-dom";
import { Fragment, useEffect } from "react";
import axios from "axios";
import Swal from "sweetalert2";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function AddOrEdit() {
  const [checkedPosition, setCheckedPosition] = useState([]);
  const [checkedPublic, setCheckedPublic] = useState("");
  const [post, setPost] = useState({});
  const baseURL = "http://localhost:3000/blogs";
  let { id } = useParams();

  useEffect(() => {
    getPostById(id);
  },[]);

  /**
   * Get post by id
   * @param {*} id
   * @returns mix
   */
  const getPostById = (id) => {
    if (id) {
      axios
        .get(`${baseURL}/${id}`, {
          params: {
            id: id,
          },
        })
        .then(function (response) {
          //set data to post
          setPost(response.data.data);
          //convert position from string to integer array
          let dataPositon = response.data.data.position
            .split(",")
            .map(function (item) {
              return parseInt(item, 10);
            });
          //set checked positon by id
          setCheckedPosition([...dataPositon]);
          //set checked public
          setCheckedPublic(response.data.data.public);
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  };

  /**
   * Handle check position
   * @param {*} id
   * @returns array
   */
  const handleCheckPosition = (id) => {
    setCheckedPosition((prev) => {
      const isChecked = checkedPosition.includes(id);
      if (isChecked) {
        return checkedPosition.filter((item) => item !== id);
      } else {
        return [...prev, id];
      }
    });
  };

  /**
   * Handle check public
   * @param {*} id
   * @returns number
   */
  const handleCheckPublic = (id) => {
    return setCheckedPublic(id);
  };

  /**
   * Handle submit
   * @param {*} event
   * @returns mix
   */
  const handleSubmit = (event) => {
    event.preventDefault();
    const post = {
      title: event.target.title.value,
      des: event.target.des.value,
      detail: event.target.detail.value,
      category: parseInt(event.target.category.value),
      public: parseInt(event.target.public.value),
      data_pubblic: event.target.data_pubblic.value,
      position: checkedPosition.toString(),
      thumbs: event.target.thumbs.value.split(/(\\|\/)/g).pop(),
    };

    if (id) {
      axios
        .put(`${baseURL}/${id}`, post)
        .then((response) => {
          if (response.status === 200) {
            Swal.fire("Cập nhật", "Cập nhật bài viết thành công!", "success");
          }
        })
        .catch((error) => {
          Object.values(error.response.data.errors).map((value, key) => {
            toast(value[0]);
          });
        });
    } else {
      axios
        .post(`${baseURL}`, post)
        .then((response) => {
          if (response.status === 201) {
            Swal.fire("Thêm mới", "Thêm mới bài viết thành công!", "success");
          }
        })
        .catch((error) => {
          Object.values(error.response.data.errors).map((value, key) => {
            toast(value[0]);
          });
        });
    }
  };

  return (
    <div>
      <Fragment>
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        {/* Same as */}
        <ToastContainer />
        <div className="">
          <AppHeader />
          <div className="row">
            <div className="col-xl-2 col-md-3 appmenu">
              <AppMenu />
            </div>
            <div className="col-xl-10 col-md-9 content">
              <div className="card">
                <div className="card-header">
                  <h3>New Blog</h3>
                </div>
                <form onSubmit={handleSubmit}>
                  <div className="card-body">
                    <div className="form-group">
                      <label>Tiêu đề</label>
                      <input
                        type="text"
                        name="title"
                        className="form-control"
                        defaultValue={post.title}
                      />
                    </div>
                    <br />

                    <div className="form-group">
                      <label>Mô tả ngắn</label>
                      <textarea
                        name="des"
                        className="form-control"
                        defaultValue={post.des}
                      />
                    </div>
                    <br />

                    <div className="form-group">
                      <label>Chi tiết</label>
                      <textarea
                        name="detail"
                        className="form-control"
                        defaultValue={post.detail}
                      />
                    </div>
                    <br />

                    <div className="form-group">
                      <label>Hình ảnh</label>
                      <input
                        type="file"
                        name="thumbs"
                        className="form-control"
                      />
                    </div>
                    <br />

                    <div className="form-group">
                      <label>Vị trí {console.log(checkedPosition)}</label>
                      <br />
                      {LIST_POSITION.map((value, key) => {
                        return (
                          <div
                            className="form-check form-check-inline"
                            key={key}
                          >
                            <input
                              className="form-check-input"
                              type="checkbox"
                              id={`position-${value.id}`}
                              value="option1"
                              name="position"
                              checked={checkedPosition.includes(value.id)}
                              onChange={() => handleCheckPosition(value.id)}
                            />
                            <label
                              className="form-check-label"
                              htmlFor={`position-${value.id}`}
                            >
                              {value.name}
                            </label>
                          </div>
                        );
                      })}
                    </div>
                    <br />

                    <div className="form-group">
                      <label>Public</label>
                      <br />
                      <div className="form-check form-check-inline">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="public"
                          id="1"
                          value="1"
                          checked={checkedPublic === 1}
                          onChange={() => handleCheckPublic(1)}
                        />
                        <label className="form-check-label" htmlFor="1">
                          Yes
                        </label>
                      </div>
                      <div className="form-check form-check-inline">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="public"
                          id="0"
                          value="0"
                          checked={checkedPublic === 0}
                          onChange={() => handleCheckPublic(0)}
                        />
                        <label className="form-check-label" htmlFor="0">
                          No
                        </label>
                      </div>
                    </div>
                    <br />

                    <div className="row">
                      <div className="col-md-6 col-12">
                        <div className="form-group">
                          <label>Loại</label>
                          <select className="form-control" name="category">
                            {LIST_CATEGORY.map((value, key) => {
                              if (value.id === post.category) {
                                return (
                                  <option key={key} value={value.id} selected>
                                    {value.name}
                                  </option>
                                );
                              } else {
                                return (
                                  <option key={key} value={value.id}>
                                    {value.name}
                                  </option>
                                );
                              }
                            })}
                          </select>
                        </div>
                      </div>
                      <div className="col-md-6 col-12">
                        <div className="form-group">
                          <label>Date Public</label>
                          <input
                            type="date"
                            name="data_pubblic"
                            className="form-control"
                            defaultValue={post.data_pubblic}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card-footer text-center">
                    <button
                      type="submit"
                      className="btn btn-success btn-submit"
                    >
                      Submit
                    </button>
                    <button type="reset" className="btn btn-primary">
                      Clear
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <AppFooter />
        </div>
      </Fragment>
    </div>
  );
}

type alphanumeric = string | number;

function add(a: alphanumeric, b: alphanumeric) {
    if (typeof a === 'number' && typeof b === 'number') {
        return a + b;
    }

    if (typeof a === 'string' && typeof b === 'string') {
        return a.concat(b);
    }

    throw new Error('Invalid arguments. Both arguments must be either numbers or strings.');
}

// Ví dụ 2
class Customer2 {
    isCreditAllowed(): boolean {
        // ...
        return true;
    }
}

class Supplier {
    isInShortList(): boolean {
        // ...
        return true;
    }
}

type BusinessPartner2 = Customer2 | Supplier;

function signContract(partner: BusinessPartner2) : string {
    let message: string;
    if (partner instanceof Customer2) {
        message = partner.isCreditAllowed() ? 'Sign a new contract with the customer' : 'Credit issue';
    }

    if (partner instanceof Supplier) {
        message = partner.isInShortList() ? 'Sign a new contract the supplier' : 'Need to evaluate further';
    }

    return message;
}

// function signContract(partner: BusinessPartner): string {
//     let message: string;
//     if (isCustomer(partner)) {
//         message = partner.isCreditAllowed() ? 'Sign a new contract with the customer' : 'Credit issue';
//     } else {
//         message = partner.isInShortList() ? 'Sign a new contract with the supplier' : 'Need to evaluate further';
//     }

//     return message;
// }

// Bảo vệ kiểu thu hẹp kiểu của một biến trong một khối có điều kiện.
// Sử dụng toán tử typeof và instanceof để triển khai các bộ bảo vệ kiểu trong các khối điều kiện
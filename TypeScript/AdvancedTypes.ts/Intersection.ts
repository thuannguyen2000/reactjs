// let varName = typeA | typeB; // union type

// type typeAB = typeA & typeB;

interface BusinessPartner {
    name: string;
    credit: number;
}

interface Identity {
    id: number;
    name: string;
}

interface Contact {
    email: string;
    phone: string;
}

type Employee6 = Identity & Contact & BusinessPartner;
type Customer = BusinessPartner & Contact;

let e: Employee6 = {
    id: 100,
    name: 'John Doe',
    email: 'john.doe@example.com',
    phone: '(408)-897-5684',
    credit: 1000
};

let c: Customer = {
    name: 'ABC Inc.',
    credit: 1000000,
    email: 'sales@abcinc.com',
    phone: '(408)-897-5735'
};

// Một kiểu giao lộ kết hợp hai hoặc nhiều kiểu để tạo ra một kiểu mới có tất cả các thuộc tính của các kiểu hiện có.
// Thứ tự loại không quan trọng khi bạn kết hợp các loại.
function getNetPrice(price: number, discount: number, format: boolean): number | string {
    let netPrice = price * (1 - discount);
    return format ? `$${netPrice}` : netPrice;
}

let netPrice = getNetPrice(100, 0.05, true) as string;
console.log(netPrice);

let netPrice2 = getNetPrice(100, 0.05, false) as number;
console.log(netPrice2);

// Các xác nhận kiểu hướng dẫn trình biên dịch coi một giá trị là một kiểu được chỉ định.
// Xác nhận kiểu không mang bất kỳ chuyển đổi kiểu nào.
// Loại xác nhận sử dụng as từ khóa hoặc cú pháp dấu ngoặc nhọn <>.
function getRandomNumberElement(items: number[]): number {
    let randomIndex = Math.floor(Math.random() * items.length);
    return items[randomIndex];
}

let numbers = [1, 5, 7, 4, 2, 9];
console.log(getRandomNumberElement(numbers));
// Ví dụ 2
function getRandomStringElement(items: string[]): string {
    let randomIndex = Math.floor(Math.random() * items.length);
    return items[randomIndex];
}
let colors = ['red', 'green', 'blue'];
console.log(getRandomStringElement(colors));
// Ví dụ 3
function getRandomAnyElement(items: any[]): any {
    let randomIndex = Math.floor(Math.random() * items.length);
    return items[randomIndex];
}

let numbers2 = [1, 5, 7, 4, 2, 9];
let colors2 = ['red', 'green', 'blue'];

console.log(getRandomAnyElement(numbers2));
console.log(getRandomAnyElement(colors2));

// Ví dụ 4
let result = merge(
    { name: 'John' },
    { jobTitle: 'Frontend Developer' }
);

console.log(result);

function merge(arg0: { name: string; }, arg1: { jobTitle: string; }) {
    throw new Error("Function not implemented.");
}

// Sử dụng các khái niệm chung của TypeScript để phát triển các hàm, giao diện và lớp có thể tái sử dụng, tổng quát và an toàn cho kiểu.
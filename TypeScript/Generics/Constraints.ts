// let person = merge(
//     { name: 'John' },
//     { age: 25 }
// );

// console.log(result);
function prop<T, K extends keyof T>(obj: T, key: K) {
    return obj[key];
}

let str = prop({ name: 'John' }, 'name');
console.log(str);

// Sử dụng extendstừ khóa để giới hạn tham số kiểu thành một kiểu cụ thể.
// Sử dụng extends keyofđể ràng buộc một kiểu là thuộc tính của một đối tượng khác.
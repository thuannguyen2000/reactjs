var counter4 = 0;
while (counter < 5) {
    console.log(counter4);
    counter4++;
}
// Ví dụ 2
var list = document.querySelector('#list');
while (list.firstChild) {
    list.removeChild(list.firstChild);
}
// Sử dụng câu lệnh TypeScript while để tạo một vòng lặp sẽ chạy miễn là có điều kiện true

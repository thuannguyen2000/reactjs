// Một if câu lệnh thực hiện một câu lệnh dựa trên một điều kiện. Nếu điều kiện là true, ifcâu lệnh sẽ thực thi các câu lệnh bên trong phần thân của nó
var max = 100;
var counter = 19;
if (counter < max) {
    counter++;
}
console.log(counter);
// IF Else
var max2 = 100;
var counter2 = 100;
if (counter2 < max2) {
    counter2++;
}
else {
    counter2 = 1;
}
console.log(counter2);
// Toán tử bậc ba
var max3 = 100;
var counter3 = 100;
counter3 < max ? counter3++ : counter3 = 1;
console.log(counter3);
// If else if
var discount;
var itemCount = 11;
if (itemCount > 0 && itemCount <= 5) {
    discount = 5;
}
else if (itemCount > 5 && itemCount <= 10) {
    discount = 10;
}
else if (itemCount > 10) {
    discount = 15;
}
else {
    throw new Error('The number of items cannot be negative!');
}
console.log("You got ".concat(discount, "% discount."));

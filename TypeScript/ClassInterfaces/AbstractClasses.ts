abstract class Employee7 {
    constructor(private firstName: string, private lastName: string) {
    }
    abstract getSalary(): number
    get fullName(): string {
        return `${this.firstName} ${this.lastName}`;
    }
    compensationStatement(): string {
        return `${this.fullName} makes ${this.getSalary()} a month.`;
    }
}

class FullTimeEmployee extends Employee7 {
    constructor(firstName: string, lastName: string, private salary: number) {
        super(firstName, lastName);
    }
    getSalary(): number {
        return this.salary;
    }
}

class Contractor extends Employee7 {
    constructor(firstName: string, lastName: string, private rate: number, private hours: number) {
        super(firstName, lastName);
    }
    getSalary(): number {
        return this.rate * this.hours;
    }
}

let john3 = new FullTimeEmployee('John', 'Doe', 12000);
let jane3 = new Contractor('Jane', 'Doe', 100, 160);

console.log(john3.compensationStatement());
console.log(jane3.compensationStatement());

// Các lớp trừu tượng không thể được khởi tạo.
// Một lớp trừu tượng có ít nhất một phương thức trừu tượng.
// Để sử dụng một lớp trừu tượng, bạn cần kế thừa nó và cung cấp cách triển khai cho các phương thức trừu tượng.
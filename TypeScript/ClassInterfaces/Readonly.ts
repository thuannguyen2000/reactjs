class Person5 {
    readonly birthDate: Date;

    constructor(birthDate: Date) {
        this.birthDate = birthDate;
    }
}

// Sử dụng công cụ sửa đổi quyền truy cập chỉ đọc để đánh dấu thuộc tính lớp là bất biến.
// Thuộc tính chỉ đọc phải được khởi tạo như một phần của khai báo hoặc trong phương thức khởi tạo của cùng một lớp.

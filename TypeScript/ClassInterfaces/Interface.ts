function getFullName(person4: {
    firstName: string;
    lastName: string
}) {
    return `${person4.firstName} ${person4.lastName}`;
}

let person4 = {
    firstName: 'John',
    lastName: 'Doe'
};

console.log(getFullName(person4));

interface Person5 {
    firstName: string;
    lastName: string;
}  

function getFullName2(person: Person5) {
    return `${person.firstName} ${person.lastName}`;
}

let john4 = {
    firstName: 'John',
    lastName: 'Doe'
};

console.log(getFullName(john4));

// Các loại chức năng
interface StringFormat {
    (str: string, isUpper: boolean): string
}

let format: StringFormat;

format = function (str: string, isUpper: boolean) {
    return isUpper ? str.toLocaleUpperCase() : str.toLocaleLowerCase();
};

console.log(format('hi', true));

// Các loại lớp

// interface Json {
//     toJSON(): string
//  }

//  class Person5 implements Json {
//     constructor(private firstName: string,
//         private lastName: string) {
//     }
//     toJson(): string {
//         return JSON.stringify(this);
//     }
// }

// Các giao diện TypeScript xác định các hợp đồng trong mã của bạn và cung cấp các tên rõ ràng để kiểm tra kiểu.
// Các giao diện có thể có các thuộc tính tùy chọn hoặc các thuộc tính chỉ đọc.
// Các giao diện có thể được sử dụng như các kiểu chức năng.
// Các giao diện thường được sử dụng như các loại lớp tạo hợp đồng giữa các lớp không liên quan.

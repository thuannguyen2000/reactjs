class Employee4 {
    static headcount: number = 0;

    constructor(
        private firstName: string,
        private lastName: string,
        private jobTitle: string) {

        Employee4.headcount++;
    }
}

let john = new Employee4('John', 'Doe', 'Front-end Developer');
let jane = new Employee4('Jane', 'Doe', 'Back-end Developer');

console.log(Employee4.headcount); // 2

// Phương pháp tĩnh

class Employee5 {
    private static headcount: number = 0;

    constructor(
        private firstName: string,
        private lastName: string,
        private jobTitle: string) {

            Employee5.headcount++;
    }

    public static getHeadcount() {
        return Employee5.headcount;
    }
}

let john2 = new Employee5('John', 'Doe', 'Front-end Developer');
let jane2 = new Employee5('Jane', 'Doe', 'Back-end Developer');

console.log(Employee5.getHeadcount); // 2

// Các thuộc tính và phương thức static được chia sẻ bởi tất cả các trường hợp của một lớp.
// Sử dụng statictừ khóa trước một thuộc tính hoặc một phương thức để làm cho nó tĩnh.
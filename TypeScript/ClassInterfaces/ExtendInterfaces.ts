interface Mailable {
    send(email: string): boolean
    queue(email: string): boolean
}

interface FutureMailable extends Mailable {
    later(email: string, after: number): boolean
}

interface A {
    a(): void
}

interface B extends A {
    b(): void
}

interface C {
    c(): void
}

interface D extends B, C {
    d(): void
}

class Mail implements FutureMailable {
    later(email: string, after: number): boolean {
        console.log(`Send email to ${email} in ${after} ms.`);
        return true;
    }
    send(email: string): boolean {
        console.log(`Sent email to ${email} after ${after} ms. `);
        return true;
    }
    queue(email: string): boolean {
        console.log(`Queue an email to ${email}.`);
        return true;
    }
}

class Control {
    private state: boolean;
}

interface StatefulControl extends Control {
    enable(): void
}

class Button extends Control implements StatefulControl {
    enable() { }
}
class TextBox extends Control implements StatefulControl {
    enable() { }
}
class Label extends Control { }


// Error: cannot implement
// class Chart implements StatefulControl {
//     enable() { }

// }

// Một giao diện có thể mở rộng một hoặc nhiều giao diện hiện có.
// Một giao diện cũng có thể mở rộng một lớp. Nếu lớp chứa các thành viên riêng tư hoặc được bảo vệ, giao diện chỉ có thể được thực thi bởi lớp hoặc các lớp con của lớp đó.
function Person(ssn, firstName, lastName) {
    this.ssn = ssn;
    this.firstName = firstName;
    this.lastName = lastName;
}

Person.prototype.getFullName = function () {
    return `${this.firstName} ${this.lastName}`;
}

let person = new Person('171-28-0926','John','Doe');
console.log(person.getFullName());

// Ví dụ 2
class Person2 {
    ssn;
    firstName;
    lastName;

    constructor(ssn, firstName, lastName) {
        this.ssn = ssn;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    getFullName() {
        return `${this.firstName} ${this.lastName}`;
    }
}

let person2 = new Person2('171-28-0926','John','Doe');
console.log(person2.getFullName());

// Sử dụng classtừ khóa để xác định một lớp trong TypeScript.
// TypeScript tận dụng cú pháp của lớp ES6 và thêm các chú thích kiểu để làm cho lớp mạnh mẽ hơn.
// Kế thừa
class Person6 {
    constructor(private firstName: string, private lastName: string) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    getFullName(): string {
        return `${this.firstName} ${this.lastName}`;
    }
    describe(): string {
        return `This is ${this.firstName} ${this.lastName}.`;
    }
}

class Employee extends Person6 {
    //..
}

// Contructor
class Employee2 extends Person6 {
    constructor(
        firstName: string,
        lastName: string,
        private jobTitle: string) {
        
        // call the constructor of the Person class:
        super(firstName, lastName);
    }
}

let employee = new Employee2('John', 'Doe', 'Web Developer');

console.log(employee.getFullName());
console.log(employee.describe());

// Ghi dè

class Employee3 extends Person6 {
    constructor(
        firstName: string,
        lastName: string,
        private jobTitle: string) {

        super(firstName, lastName);
    }

    describe(): string {
        return super.describe() + `I'm a ${this.jobTitle}.`;
    }
}

let employee2 = new Employee3('John', 'Doe', 'Web Developer');
console.log(employee2.describe());

// Sử dụng extends từ khóa để cho phép một lớp kế thừa từ một lớp khác.
// Sử dụng super()trong phương thức khởi tạo của lớp con để gọi phương thức khởi tạo của lớp cha. Ngoài ra, hãy sử dụng super.methodInParentClass()cú pháp để gọi methodInParentClass()phương thức trong phương thức của lớp con. 
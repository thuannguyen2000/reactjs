class Person3 {
    private ssn: string;
    private firstName: string;
    private lastName: string;

    constructor(ssn: string, firstName: string, lastName: string) {
        this.ssn = ssn;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    getFullName(): string {
        return `${this.firstName} ${this.lastName}`; 
    }
}

// Protected

class Person4 {
    constructor(protected ssn: string, private firstName: string, private lastName: string) {
        this.ssn = ssn;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    getFullName(): string {
        return `${this.firstName} ${this.lastName}`;
    }
}

// TypeScript cung cấp ba công cụ sửa đổi quyền truy cập cho các thuộc tính và phương thức của lớp private:, protectedvà public.
// Công private cụ sửa đổi cho phép truy cập trong cùng một lớp.
// Công protected cụ sửa đổi cho phép truy cập trong cùng một lớp và các lớp con.
// Công publiccụ sửa đổi cho phép truy cập từ bất kỳ vị trí nào.
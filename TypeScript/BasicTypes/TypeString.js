var firstName = 'John';
var title = "Web Developer";
var description = "This TypeScript string can\nspan multiple\nlines\n";
console.log(firstName);
console.log(title);
console.log(description);
// Nội suy
var profile = "I'm ".concat(firstName, ".\nI'm a ").concat(title, ".");
console.log(profile);

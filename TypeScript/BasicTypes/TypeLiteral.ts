// let mouseEvent: 'click' | 'dblclick' | 'mouseup' | 'mousedown';
// mouseEvent = 'click'; // valid
// mouseEvent = 'dblclick'; // valid
// mouseEvent = 'mouseup'; // valid
// mouseEvent = 'mousedown'; // valid
// mouseEvent = 'mouseover'; // compiler error

// type MouseEvent: 'click' | 'dblclick' | 'mouseup' | 'mousedown';
// let mouseEvent: MouseEvent;
// mouseEvent = 'click'; // valid
// mouseEvent = 'dblclick'; // valid
// mouseEvent = 'mouseup'; // valid
// mouseEvent = 'mousedown'; // valid
// mouseEvent = 'mouseover'; // compiler error

// let anotherEvent: MouseEvent;

// Kiểu ký tự chuỗi TypeScript xác định kiểu chấp nhận ký tự chuỗi được chỉ định.
// Sử dụng các kiểu ký tự chuỗi với các kiểu liên hợp và bí danh kiểu để xác định các kiểu chấp nhận một tập hợp hữu hạn các ký tự chuỗi.
// let message: string = 'Hello Word';
// console.log(message);

// let heading = document.createElement('h1')
// heading.textContent = message;
// document.body.appendChild(heading);


// interface Product {
//     id: number,
//     name: string,
//     price: number
// };

// function getProduct(id) : Product {
//     return {
//         id: id,
//         name: `Thuận Nguyễn ${id}`,
//         price: 20000
//     }
// }

// const product = getProduct(18);
// console.log(`The product ${product.name} costs ${product.price}`);

// Chú thích kiểu trong TypeScrpit

// let sum : number = 2 + 2;
// let FullName : string = 'Thuan Nguyen';
// let Yes : boolean = true;

// console.log(sum);
// console.log(FullName);
// console.log(Yes);

// Chú thích kiểu trong mảng
// let person: string[] = ['John','Mary','David'];

// let names: {
//     name: string;
//     age: number
// };

// names = {
//     name: 'John',
//     age: 25
// };

// console.log(person);
// console.log(names);

// Suy luận kiểu typescript

function increment(counter: number) {
    return ++counter;
}

console.log(increment(3));


// Nhập theo ngữ cảnh
// document.addEventListener('click', function(e) {
//     console.log(e.button);
    
// })

// Kiểu dữ liệu số
let price: number = 9.3;

// Số thập phân
// let number: number;
let x: number = 100,
    y: number = 200;

console.log(x + y);


// Số nhị phân
let bin = 0b100;
let anotherBin: number = 0B010;

// Số bát phân
// Sau 0o là số trong phạm vi 0 -> 7
let octal: number = 0o1010;

// Số thập lục phân
// Các chữ số sau dấu 0xphải nằm trong khoảng ( 0123456789ABCDEF)
let hexadecimal: number = 0XA;

// số nguyên lớn  
let big: bigint = 96429816492169216894621964921n;   
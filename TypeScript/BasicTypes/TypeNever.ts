// Không trả về giá trị nào 
function raiseError(message: string):never {
    throw new Error(message);
}

function reject() {
    return raiseError('Rejected')
}

console.log(raiseError);

let loop = function forever() {
    while (true) {
        console.log('Hello');
    }
}

function fn(a: string | number): boolean {
    if(typeof a === "string") {
        return true;
    } else if (typeof a === "number") {
        return false;
    }
    return neverOccur();
    }
    let neverOccur = () => {
        throw new Error('Never!');
    }

//  Loại never không chứa giá trị.
// Kiểu never đại diện cho kiểu trả về của một hàm luôn tạo ra lỗi hoặc một hàm chứa một vòng lặp không xác định.
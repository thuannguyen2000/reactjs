// Không trả về giá trị nào 
function raiseError(message) {
    throw new Error(message);
}
function reject() {
    return raiseError('Rejected');
}
console.log(raiseError);
var loop = function forever() {
    while (true) {
        console.log('Hello');
    }
};
function fn(a) {
    if (typeof a === "string") {
        return true;
    }
    else if (typeof a === "number") {
        return false;
    }
    return neverOccur();
}
var neverOccur = function () {
    throw new Error('Never!');
};

// Kiểu liệt kê
enum Month {
    Jan = 1,
    Feb,
    Mar,
    Apr,
    May,
    Jun,
    Jul,
    Aug,
    Sep,
    Oct,
    Nov,
    Dec
}

console.log(Month);

// Sử dụng Enum trong trường hợp
// 1.Có một tập hợp nhỏ các giá trị cố định có liên quan chặt chẽ với nhau
// 2.Những giá trị này đã được biết trước

enum ApprovalStatus {
    draft,
    submitted,
    approved,
    rejected
};

const request = {
    id: 1,
    status: ApprovalStatus.approved,
    description: 'Please approve this request'
};

if(request.status === ApprovalStatus.approved) {
    console.log('Send email to the Applicant...');
    
}


// let message: string = 'Hello Word';
// console.log(message);
// let heading = document.createElement('h1')
// heading.textContent = message;
// document.body.appendChild(heading);
// interface Product {
//     id: number,
//     name: string,
//     price: number
// };
// function getProduct(id) : Product {
//     return {
//         id: id,
//         name: `Thuận Nguyễn ${id}`,
//         price: 20000
//     }
// }
// const product = getProduct(18);
// console.log(`The product ${product.name} costs ${product.price}`);
// Chú thích kiểu trong TypeScrpit
// let sum : number = 2 + 2;
// let FullName : string = 'Thuan Nguyen';
// let Yes : boolean = true;
// console.log(sum);
// console.log(FullName);
// console.log(Yes);
// Chú thích kiểu trong mảng
// let person: string[] = ['John','Mary','David'];
// let names: {
//     name: string;
//     age: number
// };
// names = {
//     name: 'John',
//     age: 25
// };
// console.log(person);
// console.log(names);
// Suy luận kiểu typescript
function increment(counter) {
    return ++counter;
}
console.log(increment(3));
// Nhập theo ngữ cảnh
// document.addEventListener('click', function(e) {
//     console.log(e.button);
// })
// Kiểu dữ liệu số
var price = 9.3;
// Số thập phân
// let number: number;
var x = 100, y = 200;
console.log(x + y);

var mouseEvent;
mouseEvent = 'click'; // valid
mouseEvent = 'dblclick'; // valid
mouseEvent = 'mouseup'; // valid
mouseEvent = 'mousedown'; // valid
mouseEvent = 'mouseover'; // compiler error
'click' | 'dblclick' | 'mouseup' | 'mousedown';
var mouseEvent;
mouseEvent = 'click'; // valid
mouseEvent = 'dblclick'; // valid
mouseEvent = 'mouseup'; // valid
mouseEvent = 'mousedown'; // valid
mouseEvent = 'mouseover'; // compiler error
var anotherEvent;
// Kiểu ký tự chuỗi TypeScript xác định kiểu chấp nhận ký tự chuỗi được chỉ định.
// Sử dụng các kiểu ký tự chuỗi với các kiểu liên hợp và bí danh kiểu để xác định các kiểu chấp nhận một tập hợp hữu hạn các ký tự chuỗi.

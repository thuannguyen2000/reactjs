function echo(message: string): void {
    console.log(message.toUpperCase());
}

// Sử dụng chú thích kiểu cho các tham số hàm và kiểu trả về để giữ cho mã gọi nội tuyến và đảm bảo kiểm tra kiểu trong thân hàm.
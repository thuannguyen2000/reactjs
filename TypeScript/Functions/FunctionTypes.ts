// Cách 1
let add: (a: number, b: number) => number =
    function (x: number, y: number) {
        return x + y;
    };
// Cách 2
    add = function (x: number, y: number) {
        return x + y;
    };
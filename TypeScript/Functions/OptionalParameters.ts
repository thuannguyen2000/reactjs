function multiply(a: number, b: number, c?: number): number {

    if (typeof c !== 'undefined') {
        return a * b * c;
    }
    return a * b;
}

// Sử dụng parameter?: typecú pháp để làm cho một tham số là tùy chọn.
// Sử dụng biểu thức typeof(parameter) !== 'undefined'để kiểm tra xem tham số đã được khởi tạo hay chưa.
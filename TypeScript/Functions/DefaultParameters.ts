function applyDiscount(price: number, discount: number = 0.05): number {
    return price * (1 - discount);
}

console.log(applyDiscount(100)); // 95

// Ví dụ 2
function getDay(year: number = new Date().getFullYear(), month: number): number {
    let day = 0;
    switch (month) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            day = 31;
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            day = 30;
            break;
        case 2:
            // leap year
            if (((year % 4 == 0) &&
                !(year % 100 == 0))
                || (year % 400 == 0))
                day = 29;
            else
                day = 28;
            break;
        default:
            throw Error('Invalid month');
    }
    return day;
}

// Ví dụ 3 
let day = getDay(2019, 2);
console.log(day); // 28

// Ví dụ 4
let day2 = getDay(undefined, 2);
console.log(day2);

// Sử dụng cú pháp tham số mặc định parameter:=defaultValuenếu bạn muốn đặt giá trị khởi tạo mặc định cho tham số.
// Các tham số mặc định là tùy chọn.
// Để sử dụng giá trị khởi tạo mặc định của một tham số, bạn bỏ qua đối số khi gọi hàm hoặc truyền undefinedvào hàm.

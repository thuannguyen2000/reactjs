function add3(a: number | string, b: number | string): number | string {
    if (typeof a === 'number' && typeof b === 'number')
        return a + b;

    if (typeof a === 'string' && typeof b === 'string')
        return a + b;
}

// Ví dụ 2
class Counter {
    private current: number = 0;
    count(): number;
    count(target: number): number[];
    count(target?: number): number | number[] {
        if (target) {
            let values = [];
            for (let start = this.current; start <= target; start++) {
                values.push(start);
            }
            this.current = target;
            return values;
        }
        return ++this.current;
    }
}

let counter = new Counter();
console.log(counter.count()); // return a number
console.log(counter.count(20)); // return an array

// Quá tải hàm TypeScript cho phép bạn mô tả mối quan hệ giữa các kiểu tham số và kết quả của một hàm.